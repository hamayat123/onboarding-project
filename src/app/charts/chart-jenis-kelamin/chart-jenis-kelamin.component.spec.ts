import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChartJenisKelaminComponent } from './chart-jenis-kelamin.component';

describe('ChartJenisKelaminComponent', () => {
  let component: ChartJenisKelaminComponent;
  let fixture: ComponentFixture<ChartJenisKelaminComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ChartJenisKelaminComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChartJenisKelaminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
