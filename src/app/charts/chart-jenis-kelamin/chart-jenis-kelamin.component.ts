import { Component, ElementRef, Input, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { Observable } from 'rxjs';
import { DataJenisKelamin } from '../../data/data-jenis-kelamin.model';
import * as d3 from 'd3';
import * as d3Scale from 'd3-scale';
import * as d3Shape from 'd3-shape';
import { pointer } from 'd3-selection';

@Component({
  selector: 'app-chart-jenis-kelamin',
  templateUrl: './chart-jenis-kelamin.component.html',
  styleUrls: ['./chart-jenis-kelamin.component.scss'],
  encapsulation: ViewEncapsulation.None,
})

export class ChartJenisKelaminComponent implements OnInit {
  @ViewChild('chartJK')
  private chartContainer!: ElementRef;

  @Input()
  data?: Observable<DataJenisKelamin[]>;

  @Input()
  dataFilters: any;

  margin = { top: 20, right: 20, bottom: 30, left: 40 };
  contentWidth: number = 0;
  contentHeight: number = 0;
  radius: number = 0;
  middle: number = 0;

  dataset: DataJenisKelamin[] = [];

  arc: any;
  arcLabel: any;
  labelCategory: any;
  labelPercent: any;
  pie: any;
  color: any;
  svg: any;
  tooltip: any;
  g: any;

  loading: boolean = false;

  constructor() { }

  // Main Function
  ngOnInit(): void {
    this.loading = true;
    
    setTimeout(() => { 
      if (!this.data) { return; }   

      this.initData();
    }, 500);
  }

  initData(): void {
    d3.select('#chart-jenis-kelamin svg').remove();

    this.loading = true;

    this.dataset = [];
    this.data?.subscribe((d: any) => {
      Object.keys(d.data.content[this.dataFilters.index + '_per_gender']).forEach((key) => {
        this.dataset.push({
          jenis_kelamin: key,
          terkonfirmasi: d.data.content[this.dataFilters.index + '_per_gender'][key]
        });
      });
      
      this.loading = false;
      this.createChart();
    });
  }

  onResize(event: any) {
    this.createChart();
  }

  createChart(): void {
    this.initSVG();
    this.drawPie();
    this.drawLegends();
  }

  // Graphic Function
  initSVG(): void {
    d3.select('#chart-jenis-kelamin svg').remove(); 
    
    const data = this.dataset;
    const element = this.chartContainer.nativeElement;

    this.contentWidth = element.offsetWidth - this.margin.left - this.margin.right;
    this.contentHeight = element.offsetHeight - this.margin.top - this.margin.bottom;

    this.middle = Math.min(this.contentWidth, this.contentHeight) / 2;
    this.radius = Math.min(this.contentWidth, this.contentHeight) / 2 - (Math.min(this.contentWidth, this.contentHeight) / 2 * 0.1);

    this.color = d3Scale.scaleOrdinal()
      .domain(data.map(d => d.jenis_kelamin))
      .range(['#2dac55', '#f6d039'])

    this.pie = d3Shape.pie()
      .sort(null)
      .value((d: any) => d.terkonfirmasi);

    this.arc = d3Shape.arc()
      .outerRadius(this.radius * 0.8)
      .innerRadius(this.radius * 0.4);

    this.arcLabel = d3Shape.arc()
      .outerRadius(this.radius * 0.9)
      .innerRadius(this.radius * 0.9);

    this.svg = d3.select(element)
      .append('svg')
      .attr('width', element.offsetWidth)
      .attr('height', element.offsetHeight)
      .attr('viewBox', '0 0 ' + Math.min(this.contentWidth, this.contentHeight) + ' ' + Math.min(this.contentWidth, this.contentHeight))
        .append('g')
        .attr('class', 'chart-container');
      
    d3.selectAll('.jeniskelamin-tooltip').remove();
    this.tooltip = d3.select('body').append('div')
      .attr('class', 'tooltip jeniskelamin-tooltip')
      .style('opacity', 0);
  }

  drawPie(): void {
    const data = this.pie(this.dataset);

    const arc = this.svg
      .append('g')
      .attr( 'transform', 'translate(' + this.middle + ',' + (this.middle - 25) + ')')
      .attr('class', 'arc');

    const pie = arc.selectAll('.pie')
      .data(data)
      .enter()
        .append('g')
        .attr('class', 'pie');

    pie.append('path')
      .attr('d', this.arc)
      .attr('fill', (d: any) => this.color(d.data.jenis_kelamin))
      .attr('class', (d: any) => 'pie-'+d.data.jenis_kelamin)
      .style('stroke-width', '3px')
      .style('stroke', 'white')
      .on('mouseover', (event: any, d: any) => {
        this.tooltip.transition()
          .duration(100)
          .style('opacity', .9);
        this.tooltip.html('<b>' + this.titleCase(d.data.jenis_kelamin.replace('_', ' - ')) 
          + '</b><br/>Terkonfirmasi: ' 
          + this.numberFormat(d.data.terkonfirmasi) 
          + ' (<b>' + d3.format('.1%')(d.value / d3.sum(data, (d: any) => d.value )) + '</b>)')
            .style('left', (event.pageX + 10) + 'px')
            .style('top', (event.pageY - 28) + 'px');
        })
      .on('mouseout', (d: any) => {
        this.tooltip.transition()
          .duration(500)
          .style('opacity', 0);
        });

    pie.append('text')
      .style('fill', 'white')
      .style('text-anchor', 'middle')
      .style('font-size', '14px')
      .attr('transform', (d: any) => 'translate(' + this.arc.centroid(d) + ')')
      .text((d: any) => d3.format('.1%')(d.value / d3.sum(data, (d: any) => d.value )));
  }

  drawLegends(): void {
    const data = this.pie(this.dataset);

    const legends = this.svg
      .append('g')
      .attr('class', 'legends');

    const legend = legends.selectAll('.legend')
      .data(data)
      .enter()
        .append('g')
        .attr('class', 'legend')
        .attr('transform', (d: any, i: any) => `translate(${((i * this.contentWidth/4) + this.contentWidth/4 + 15)}, ${(this.radius * 2)})`);

    legend.append('circle')
      .attr('fill', (d: any) => this.color(d.data.jenis_kelamin))
      .attr('stroke', 'none')
      .attr('r', 7);

    legend.append('text')
      .style('font-size', '11')
      .attr('transform', 'translate(13, 4)')
      .text((d: any) => this.titleCase(d.data.jenis_kelamin.replace('_', ' - ')));

  }
  
  // Additional Function
  titleCase(text: string){
    return text[0].toUpperCase() + text.slice(1).toLowerCase();
  }

  numberFormat(x: any): string {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.');
  }
}
