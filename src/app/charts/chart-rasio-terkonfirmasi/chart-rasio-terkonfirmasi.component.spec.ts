import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChartRasioTerkonfirmasiComponent } from './chart-rasio-terkonfirmasi.component';

describe('ChartRasioTerkonfirmasiComponent', () => {
  let component: ChartRasioTerkonfirmasiComponent;
  let fixture: ComponentFixture<ChartRasioTerkonfirmasiComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ChartRasioTerkonfirmasiComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChartRasioTerkonfirmasiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
