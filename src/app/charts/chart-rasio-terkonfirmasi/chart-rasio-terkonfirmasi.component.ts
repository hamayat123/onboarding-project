import { Component, ElementRef, Input, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { Observable } from 'rxjs';
import * as d3 from 'd3';
import * as d3Scale from 'd3-scale';
import { pointer } from 'd3-selection';

import { DataRasio } from 'src/app/data/data-rasio.model';

@Component({
  selector: 'app-chart-rasio-terkonfirmasi',
  templateUrl: './chart-rasio-terkonfirmasi.component.html',
  styleUrls: ['./chart-rasio-terkonfirmasi.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ChartRasioTerkonfirmasiComponent implements OnInit {

  @ViewChild('chartRasio')
  private chartContainer!: ElementRef;

  @Input()
  data?: Observable<DataRasio[]>;

  @Input()
  dataFilters: any;

  margin = {top: 20, right: 20, bottom: 30, left: 50};
  contentWidth: number = 0;
  contentHeight: number = 0;

  loading: boolean = true;
  dataset: DataRasio[] = [];

  svg: any;
  g: any;
  x: any;
  y: any;
  color: any;
  xAxis: any;
  yAxis: any;
  tooltip: any;

  constructor() { }

  // Main Function
  ngOnInit(): void {
    this.loading = true;
    
    setTimeout(() => {
      if (!this.data) { return; }   

      this.initData();
    }, 500);
  }

  onResize(event: any) {
    this.createChart();
  }

  createChart(): void {
    this.initSVG();
    this.drawAxis();
    this.drawBars();
  }

  initData(): void {
    d3.select('#chart-rasio svg').remove();
    
    this.loading = true;

    this.data?.subscribe((d: any) => {
      this.dataset = d.data.map((content: any) => {
        return {
          kode_kab: content[this.dataFilters.level.kode],
          nama_kab: content[this.dataFilters.level.nama],
          terkonfirmasi: content[this.dataFilters.sebaranType.index][this.dataFilters.sebaranType.subindex]
        }
      });

      this.createChart();
      this.loading = false;
    });
  }

  // Graphic Function
  initSVG(): void {
    d3.select('#chart-rasio svg').remove();

    const element = this.chartContainer.nativeElement;
    const data = this.dataset.sort((a, b) => d3.descending(a.terkonfirmasi, b.terkonfirmasi));

    this.svg = d3.select(element).append('svg')
      .attr('width', element.offsetWidth)
      .attr('height', element.offsetHeight)
        .append('g')
        .attr('class', 'chart-container')
        .attr('transform', 'translate(' + this.margin.left + ',' + this.margin.top + ')');

    this.contentWidth = element.offsetWidth - this.margin.left - this.margin.right;
    this.contentHeight = element.offsetHeight - this.margin.top - this.margin.bottom;

    this.color = d3
      .scaleLinear()
      .domain([
        d3.min(data, (d: any) => d.terkonfirmasi) as number, 
        d3.max(data, (d: any) => d.terkonfirmasi) as number
      ])
      .range(<any>['#f5f5f5', this.dataFilters.sebaranType.color]);

    this.x = d3
      .scaleLinear()
      .range([70, this.contentWidth])
      .domain([
        d3.min(data, (d: any) => d.terkonfirmasi) as number, 
        d3.max(data, (d: any) => d.terkonfirmasi) as number
      ]);

    this.y = d3
      .scaleBand()
      .rangeRound([0, this.contentHeight])
      .domain(data.map(d => d.nama_kab))
      .padding(.5);

    this.xAxis = d3.axisBottom(this.x)
      .tickSize(-this.contentHeight)
      .ticks(6);

    this.yAxis = d3.axisLeft(this.y)
      .tickFormat((d: any) => d.replace('Kabupaten', 'Kab.'))
      .tickSize(0);

    d3.selectAll('.rasio-tooltip').remove();
    this.tooltip = d3.select('body').append('div')
      .attr('class', 'tooltip rasio-tooltip')
      .style('opacity', 0);
  }

  drawAxis(): void {
    const axis = this.svg.append('g')
      .attr('class', 'axis-container');

    axis.append('g')
      .attr('class', 'axis axis--x')
      .attr('transform', 'translate(0,' + this.contentHeight + ')')
      .call(this.xAxis);

    axis.append('g')
      .attr('class', 'axis axis--y')
      .attr('transform', 'translate(55, 0)')
      .call(this.yAxis);
  }

  drawBars(): void {
    const data = this.dataset;

    const bars = this.svg.append('g')
      .attr('class', 'bars');

    bars.selectAll('.bar')
      .data(data)
      .enter()
        .append('rect')
        .attr('class', (d: any) => 'bar bar--' + d.nama_kab.replace(' ', '_'))
        .attr('x', (d: any) => this.x(Math.min(0, d.terkonfirmasi)))
        .attr('y', (d: any) => this.y(d.nama_kab))
        .attr('width', (d: any) => Math.abs(this.x(d.terkonfirmasi) - this.x(0)))
        .attr('height', this.y.bandwidth())
        .attr('fill', (d: any) => this.color(d.terkonfirmasi))
        .on('mouseover', (event: any, d: any) => {
          this.tooltip.transition()
            .duration(100)
            .style('opacity', .9);
          this.tooltip.html('<b>' + d.nama_kab + '</b><br/>Terkonfirmasi: ' + this.numberFormat(d.terkonfirmasi))
            .style('left', (event.pageX + 16) + 'px')
            .style('top', (event.pageY - 28) + 'px');
          })
        .on('mouseout', (d: any) => {
          this.tooltip.transition()
            .duration(500)
            .style('opacity', 0);
          })
        .transition()
        .duration(1000)
        .delay(100);
  }

  drawAxisMiddle(): void {
    this.svg.append('g')
      .attr('class', 'axis axis--y axis-line')
      .attr('transform', 'translate(' + (this.x(0)) + ', 0)')
      .call(this.yAxis);
  }

  drawLegends(): void {
    const data = this.dataset;

    const legends = this.svg
      .append('g')
      .attr('class', 'legends');

    const legend = legends.selectAll('.legend')
      .data(data.slice(0, 2))
      .enter()
        .append('g')
        .attr('class', 'legend')
        .attr('transform', (d: any, i: any) => `translate(${((i * this.contentWidth/4) + this.contentWidth/4 + 15)}, ${(this.contentHeight + 30)})`);

    legend.append('rect')
      .attr('class', (d: any) => 'legend--' + d.nama_kab.replace(' ', '_'))
      .attr('stroke', 'none')
      .attr('width', 15)
      .attr('height', 12);

    legend.append('text')
      .style('font-size', '12')
      .attr('transform', 'translate(19, 10)')
      .text((d: any) => this.titleCase(d.nama_kab));

  }
  
  // Additional Function
  titleCase(d: string){
    return d[0].toUpperCase() + d.slice(1).toLowerCase();
  }

  numberFormat(d: any): string {
    return d.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.');
  }
}
