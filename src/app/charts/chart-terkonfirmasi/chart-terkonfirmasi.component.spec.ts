import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChartTerkonfirmasiComponent } from './chart-terkonfirmasi.component';

describe('ChartTerkonfirmasiComponent', () => {
  let component: ChartTerkonfirmasiComponent;
  let fixture: ComponentFixture<ChartTerkonfirmasiComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ChartTerkonfirmasiComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChartTerkonfirmasiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
