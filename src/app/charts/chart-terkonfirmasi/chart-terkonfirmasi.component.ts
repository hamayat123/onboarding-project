import { Component, ElementRef, Input, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import * as d3 from 'd3';
import { pointer } from 'd3-selection';
import { Observable } from 'rxjs';
import { DataHarian } from 'src/app/data/data-harian.model';

@Component({
  selector: 'app-chart-terkonfirmasi',
  templateUrl: './chart-terkonfirmasi.component.html',
  styleUrls: ['./chart-terkonfirmasi.component.scss'],
  encapsulation: ViewEncapsulation.None
})

export class ChartTerkonfirmasiComponent implements OnInit {
  @ViewChild('chartTerkonfirmasi')
  private chartContainer!: ElementRef;

  @Input()
  data?: Observable<DataHarian[]>;

  @Input()
  dataFilters: any;

  margin = {top: 20, right: 20, bottom: 30, left: 40};
  contentWidth: number = 0;
  contentHeight: number = 0;

  loading: boolean = true;
  lastWeek: number[] = [];
  dataset: DataHarian[] = [];

  svg: any;
  g: any;
  x: any;
  y: any;
  xAxis: any;
  yAxis: any;
  yAxisGrid: any;
  valueline: any;
  tooltip: any;

  constructor() {
  }
  
  // Main Function
  ngOnInit(): void {
    this.loading = true;
    
    setTimeout(() => {
      if (!this.data) { return; }    

      this.initData();
    }, 500);
  }

  onResize(event: any) {
    this.createChart();
  }

  createChart(): void {
    this.initSVG();
    this.drawGrid();
    this.drawAxis();
    this.drawBar();
    this.drawLabel();
    this.drawLine();
  }

  initData(): void {
    d3.select('#chart-terkonfirmasi svg').remove();

    this.loading = true;

    this.data?.subscribe((d: any) => {
      this.dataset = d.data.content.map((content: any, key: any) => {
        return {
          terkonfirmasi: content.CONFIRMATION, 
          tanggal: content.tanggal,
          tanggal_formatted: this.reformatDate(content.tanggal),
          seminggu_terakhir: this.weeklyAverage(content.CONFIRMATION)
        }
      }).filter((content: any) => this.filterDate(content.tanggal));
      
      this.loading = false;
      this.createChart();
    });
  }

  // Graphic Function
  initSVG(): void {
    d3.select('#chart-terkonfirmasi svg').remove();
    
    const element = this.chartContainer.nativeElement;
    const data = this.dataset;

    this.svg = d3.select(element).append('svg')
      .attr('width', element.offsetWidth)
      .attr('height', element.offsetHeight+50);

    this.g = this.svg.append('g')
      .attr('transform', 'translate(' + this.margin.left + ',' + this.margin.top + ')');

    this.contentWidth = element.offsetWidth - this.margin.left - this.margin.right;
    this.contentHeight = element.offsetHeight - this.margin.top - this.margin.bottom;

    this.x = d3
      .scaleBand()
      .range([20, this.contentWidth])
      .padding(0.5)
      .domain(data.map((d: any) => d.tanggal_formatted));

    this.y = d3
      .scaleLinear()
      .rangeRound([this.contentHeight, 0])
      .domain([0, d3.max(data, d => d.terkonfirmasi) as number]);

    this.xAxis = d3
      .axisBottom(this.x)
      .tickValues(this.x.domain().filter((d: any, i: any) => !(i%9)));

    this.yAxis = d3.axisLeft(this.y)
      .tickSize(-this.contentWidth)
      .ticks(5);

    this.yAxisGrid = d3.axisLeft(this.y)
      .tickSize(-this.contentWidth)
      .ticks(10)
      .tickFormat((d: any) =>  '');

    this.valueline = d3
      .line()
      .x((d: any) => this.x(d.tanggal_formatted) || 0)
      .y((d: any) => this.y(d.seminggu_terakhir) || 0)
      .curve(d3.curveMonotoneX);

    d3.selectAll('.harian-tooltip').remove();
    this.tooltip = d3.select('body').append('div')
      .attr('class', 'tooltip harian-tooltip')
      .style('opacity', 0);
  }

  drawGrid(): void {
    this.g.append('g')
      .attr('class', 'grid grid--horizontal')
      .call(this.yAxisGrid)
  }

  drawAxis(): void {
    // bottom
    this.g.append('g')
      .attr('class', 'axis axis--x')
      .attr('transform', 'translate(0,' + this.contentHeight + ')')
      .call(this.xAxis)
      .selectAll('text')	
        .style('text-anchor', 'start')
        .attr('dx', '.9em')
        .attr('dy', '.15em')
        .attr('transform', 'rotate(65)');

    // left
    this.g.append('g')
      .attr('class', 'axis axis--y')
      .call(this.yAxis)
      .append('text')
        .attr('transform', 'rotate(-90)')
        .attr('y', 6)
        .attr('dy', '0.71em')
        .attr('text-anchor', 'end')
        .text('terkonfirmasi');
  }

  drawBar(): void {
    const data = this.dataset;

    this.g.selectAll('.bar')
      .data(data)
      .enter().append('rect')
        .attr('class', 'bar')
        .attr('x', (d: any) => this.x(d.tanggal_formatted) || 0)
        .attr('y', (d: any) => this.y(d.terkonfirmasi) || 0)
        .attr('width', this.x.bandwidth())
        .attr('height', (d: any) => this.contentHeight - this.y(d.terkonfirmasi))
        .on('mouseover', (event: any, d: any) => {
          this.tooltip.transition()
            .duration(100)
            .style('opacity', .9);
          this.tooltip.html('<b>' + d.tanggal_formatted + '</b><br/>Terkonfirmasi: ' + this.numberFormat(d.terkonfirmasi) + '<br/>Rata - rata 7 hari: ' + this.numberFormat(d.seminggu_terakhir))
            .style('left', (event.pageX + 10) + 'px')
            .style('top', (event.pageY - 28) + 'px');
          })
        .on('mouseout', (d: any) => {
          this.tooltip.transition()
            .duration(500)
            .style('opacity', 0);
          })
        .transition()
        .duration(1000)
        .delay(100); 
  }

  drawLine(): void {
    const data = this.dataset;

    this.g.append('path')
      .data([data])
      .attr('class', 'line')
      .attr('d', <any>this.valueline);
    
    this.g.selectAll('.dot')
      .data(data)
      .enter().append('circle') // Uses the enter().append() method
        .attr('class', 'dot') // Assign a class for styling
        .attr('cx', (d: any) => this.x(d.tanggal_formatted) || 0 )
        .attr('cy', (d: any) => this.y(d.seminggu_terakhir) || 0)
        .attr('r', 5)
        .on('mouseover', (event: any, d: any) => {
          this.tooltip.transition()
            .duration(100)
            .style('opacity', .9);
          this.tooltip.html('<b>' + d.tanggal_formatted + '</b><br/>Terkonfirmasi: ' + this.numberFormat(d.terkonfirmasi) + '<br/>Rata - rata 7 hari: ' + this.numberFormat(d.seminggu_terakhir))
            .style('left', (event.pageX + 10) + 'px')
            .style('top', (event.pageY - 28) + 'px');
          })
        .on('mouseout', (d: any) => {
          this.tooltip.transition()
            .duration(500)
            .style('opacity', 0);
          });
  }

  drawLabel(): void{
    const data = this.dataset;

    this.g.selectAll('.text')
      .data(data)
      .enter()
      .append('text')
      .attr('class','chart-label')
      .attr('x', (d: any) => this.x(d.tanggal_formatted) || 0)
      .attr('y', (d: any) => (this.y(d.terkonfirmasi)) || 0)
      .attr('text-anchor', 'middle')
      .text((d: any) => this.numberFormat(d.terkonfirmasi)); 
  }

  // Additional Function
  reformatDate(date: any): string {
    const dateArr = date.split('-');
    const months = ['', 'Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Agu', 'Sep', 'Okt', 'Nov', 'Des'];
    const reformat = `${dateArr[2]} ${months[(dateArr[1] * 1)]} ${dateArr[0]}`;

    return reformat;
  }

  weeklyAverage(confirmed: any): number {
    let weeklyAvg = 0;
    let temp = [];

    this.lastWeek.push(confirmed);

    if (this.lastWeek.length > 7) {
      this.lastWeek.shift();
    }

    temp = this.lastWeek;
    weeklyAvg = Math.floor(temp.reduce((a, b) => a + b, 0) / this.lastWeek.length);

    return weeklyAvg;
  }

  numberFormat(x: any): string {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.');
  }

  filterDate(tanggal: any): boolean{
    const arrDate = tanggal.split('-');
    const newDate = `${arrDate[1]}/${arrDate[2]}/${arrDate[0]}`;
    const curDate = new Date(newDate);

    const fromDate = new Date(`${this.dataFilters.range.from.month}/${this.dataFilters.range.from.day}/${this.dataFilters.range.from.year}`);
    const toDate = new Date(`${this.dataFilters.range.to.month}/${this.dataFilters.range.to.day}/${this.dataFilters.range.to.year}`);

    return curDate.getTime() >= fromDate.getTime() && curDate.getTime() <= toDate.getTime();
  }
}
