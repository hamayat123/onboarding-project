import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChartUmurComponent } from './chart-umur.component';

describe('ChartUmurComponent', () => {
  let component: ChartUmurComponent;
  let fixture: ComponentFixture<ChartUmurComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ChartUmurComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChartUmurComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
