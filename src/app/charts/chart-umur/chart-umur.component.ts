import { Component, ElementRef, Input, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { Observable } from 'rxjs';
import { DataUmur } from 'src/app/data/data-umur.model';
import * as d3 from 'd3';
import * as d3Scale from 'd3-scale';
import * as d3Shape from 'd3-shape';

@Component({
  selector: 'app-chart-umur',
  templateUrl: './chart-umur.component.html',
  styleUrls: ['./chart-umur.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ChartUmurComponent implements OnInit {

  @ViewChild('chartUmur')
  private chartContainer!: ElementRef;

  @Input()
  data?: Observable<DataUmur[]>;

  @Input()
  dataFilters: any;

  margin = {top: 20, right: 20, bottom: 30, left: 50};
  contentWidth: number = 0;
  contentHeight: number = 0;

  loading: boolean = true;
  dataset: DataUmur[] = [];

  svg: any;
  g: any;
  x: any;
  y: any;
  xAxis: any;
  yAxis: any;
  yAxisLabel: any;
  valueline: any;
  tooltip: any;

  constructor() { }

  // Main Function
  ngOnInit(): void {
    this.loading = true;
    
    setTimeout(() => {
      if (!this.data) { return; }   

      this.initData();
    }, 500);
  }

  onResize(event: any) {
    this.createChart();
  }

  createChart(): void {
    this.initSVG();
    this.drawAxis();
    this.drawBars();
    this.drawAxisMiddle();
    this.drawLegends();
  }

  initData(): void {
    if (!this.data) { return; }   

    d3.select('#chart-umur svg').remove();

    this.loading = true;

    this.dataset = [];
    this.data?.subscribe((d: any) => {
      Object.keys(d.data.content[this.dataFilters.index + '_per_usia'][this.dataFilters.type].laki_laki).forEach((key) => {
        this.dataset.push({
          umur: key,
          jenis_kelamin: 'laki_laki',
          terkonfirmasi: 0 - d.data.content[this.dataFilters.index + '_per_usia'][this.dataFilters.type].laki_laki[key],
        });

        this.dataset.push({
          umur: key,
          jenis_kelamin: 'perempuan',
          terkonfirmasi: d.data.content[this.dataFilters.index + '_per_usia'][this.dataFilters.type].perempuan[key],
        });
      });

      this.loading = false;
      this.createChart();
    });
  }

  // Graphic Function
  initSVG(): void {
    d3.select('#chart-umur svg').remove();

    const element = this.chartContainer.nativeElement;
    const data = this.dataset;

    this.svg = d3.select(element).append('svg')
      .attr('width', element.offsetWidth)
      .attr('height', element.offsetHeight + 30)
        .append('g')
        .attr('class', 'chart-container')
        .attr('transform', 'translate(' + this.margin.left + ',' + this.margin.top + ')');

    this.contentWidth = element.offsetWidth - this.margin.left - this.margin.right;
    this.contentHeight = element.offsetHeight - this.margin.top - this.margin.bottom;

    this.x = d3
      .scaleLinear()
      .range([0, this.contentWidth])
      .domain([
        d3.min(data, (d: any) => d.terkonfirmasi) as number, 
        d3.max(data, (d: any) => d.terkonfirmasi) as number
      ]).nice();

    this.y = d3
      .scaleBand()
      .rangeRound([0, this.contentHeight])
      .domain(data.map(d => d.umur))
      .padding(.3);

    this.xAxis = d3.axisBottom(this.x)
      .ticks(9)
      .tickSize(-this.contentHeight)
      .tickFormat((d: any) => this.numberFormat(d3.format('0')(this.removeNegative(d))));

    this.yAxis = d3.axisLeft(this.y)
      .tickSize(0)
      .tickFormat((d: any) =>  '');

    this.yAxisLabel = d3.axisLeft(this.y)
      .tickSize(0)
      .tickFormat((d: any) => d.replace('bawah_', '<').replace('atas_', '>').replace('_', ' - '));

    d3.selectAll('.umur-tooltip').remove();
    this.tooltip = d3.select('body').append('div')
      .attr('class', 'tooltip umur-tooltip')
      .style('opacity', 0);
  }

  drawAxis(): void {
    const axis = this.svg.append('g')
      .attr('class', 'axis-container');

    axis.append('g')
      .attr('class', 'axis axis--x')
      .attr('transform', 'translate(0,' + this.contentHeight + ')')
      .call(this.xAxis);

    axis.append('g')
      .attr('class', 'axis axis--y axis-label')
      .attr('transform', 'translate(-10)')
      .call(this.yAxisLabel);
  }

  drawBars(): void {
    const data = this.dataset;

    const bars = this.svg.append('g')
      .attr('class', 'bars');

    bars.selectAll('.bar')
      .data(data)
      .enter()
        .append('rect')
        .attr('class', (d: any) => 'bar bar--' + d.jenis_kelamin)
        .attr('x', (d: any) => this.x(Math.min(0, d.terkonfirmasi)))
        .attr('y', (d: any) => this.y(d.umur))
        .attr('width', (d: any) => Math.abs(this.x(d.terkonfirmasi) - this.x(0)))
        .attr('height', this.y.bandwidth())
        .on('mouseover', (event: any, d: any) => {
          this.tooltip.transition()
            .duration(100)
            .style('opacity', .9);
          this.tooltip.html('<b>' + d.umur.replace('bawah_', '<').replace('atas_', '>').replace('_', ' - ') + '</b><br/>Terkonfirmasi: ' + this.numberFormat(this.removeNegative(d.terkonfirmasi)))
            .style('left', (event.pageX + 16) + 'px')
            .style('top', (event.pageY - 28) + 'px');
          })
        .on('mouseout', (d: any) => {
          this.tooltip.transition()
            .duration(500)
            .style('opacity', 0);
          })
        .transition()
        .duration(1000)
        .delay(100);
  }

  drawAxisMiddle(): void {
    this.svg.append('g')
      .attr('class', 'axis axis--y axis-line')
      .attr('transform', 'translate(' + (this.x(0)) + ', 0)')
      .call(this.yAxis);
  }

  drawLegends(): void {
    const data = this.dataset;

    const legends = this.svg
      .append('g')
      .attr('class', 'legends');

    const legend = legends.selectAll('.legend')
      .data(data.slice(0, 2))
      .enter()
        .append('g')
        .attr('class', 'legend')
        .attr('transform', (d: any, i: any) => `translate(${((i * this.contentWidth/4) + this.contentWidth/4 + 15)}, ${(this.contentHeight + 30)})`);

    legend.append('rect')
      .attr('class', (d: any) => 'legend--' + d.jenis_kelamin)
      .attr('stroke', 'none')
      .attr('width', 15)
      .attr('height', 12);

    legend.append('text')
      .style('font-size', '12')
      .attr('transform', 'translate(19, 10)')
      .text((d: any) => this.titleCase(d.jenis_kelamin.replace('_', ' - ')));

  }
  
  // Additional Function
  titleCase(d: string){
    return d[0].toUpperCase() + d.slice(1).toLowerCase();
  }

  numberFormat(d: any): string {
    return d.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.');
  }

  removeNegative(d: any): any {
    if (d === 0) 
      return '';
    else if (d < 0) 
      d = -d;

    return d;
  }
}
