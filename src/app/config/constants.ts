import { Injectable } from '@angular/core';

@Injectable()

export class Constants {
    public readonly API_ENDPOINT: string = 'https://covid19-public.digitalservice.id/api/v1';
    public readonly API_ENDPOINT_SECOND: string = 'https://dashboard-pikobar-api.digitalservice.id/v2';
}