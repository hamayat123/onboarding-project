export interface DataBerita {
  id: number;
  title: string;
  slug: string;
  date: string;
  content: string;
  img: string;
}
