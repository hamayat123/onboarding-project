export interface DataHarian {
  terkonfirmasi: number;
  tanggal: string;
  tanggal_formatted: string;
  seminggu_terakhir: number;
}
