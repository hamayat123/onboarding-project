import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MapSebaranPolygonComponent } from './map-sebaran-polygon.component';

describe('MapSebaranPolygonComponent', () => {
  let component: MapSebaranPolygonComponent;
  let fixture: ComponentFixture<MapSebaranPolygonComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MapSebaranPolygonComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MapSebaranPolygonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
