import { Component, EventEmitter, Input, OnInit, Output, ViewEncapsulation } from '@angular/core';
import { Observable } from 'rxjs';
import { latLng, Map, tileLayer } from 'leaflet';
import * as L from 'leaflet';
import * as d3 from 'd3';
import { DataRasio } from 'src/app/data/data-rasio.model';
import { faFilter, faDotCircle, faCircle } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-map-sebaran-polygon',
  templateUrl: './map-sebaran-polygon.component.html',
  styleUrls: ['./map-sebaran-polygon.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class MapSebaranPolygonComponent implements OnInit {

  @Input()
  dataMap?: Observable<any[]>;

  @Input()
  dataRasio?: Observable<any[]>;

  @Input()
  dataFilters: any;

  @Output() 
  filterSebaran = new EventEmitter<any>();

  options = {
    layers: [
      tileLayer('https://tiles.stadiamaps.com/tiles/alidade_smooth/{z}/{x}/{y}{r}.png', {
        attribution: '&copy; <a href="https://stadiamaps.com/">Stadia Maps</a>, &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors',
        maxZoom: 18,
      })
    ],
    zoom: 8,
    center: latLng([ -7.0051667, 107.2264099 ])
  };

  sebaranType: any = [{
    index : 'confirmation', 
    subindex : 'confirmation_diisolasi', 
    text: 'Positif - Isolasi / Dalam Perawatan',
    color: '#ae9420'
  }, {
    index : 'confirmation', 
    subindex : 'confirmation_selesai', 
    text: 'Positif - Selesai Isolasi / Sembuh',
    color: '#00441f'
  }, {
    index : 'confirmation', 
    subindex : 'confirmation_meninggal', 
    text: 'Positif - Meninggal',
    color: '#7d0005'
  }, {
    index : 'suspect', 
    subindex : 'suspect_diisolasi', 
    text: 'Suspek - Isolasi / Dalam perawatan',
    color: '#beab8d'
  }, {
    index : 'closecontact', 
    subindex : 'closecontact_dikarantina', 
    text: 'Kontak Erat - Masih Dikarantina',
    color: '#905a08'
  }, {
    index : 'probable', 
    subindex : 'probable_diisolasi', 
    text: 'Probable - Isolasi / Dalam Perawatan',
    color: '#ceb546'
  }, {
    index : 'probable', 
    subindex : 'probable_meninggal', 
    text: 'Probable - Meninggal',
    color: '#7d0005'
  }];

  loading: boolean = true;
  dataset: DataRasio[] = [];
  polygon: any;

  faFilter = faFilter;
  faCircle = faCircle;

  map: any;
  color: any;
  tooltip: any;
  totalKasus = 0;

  constructor() { }

  ngOnInit(): void {
    this.loading = true;
    
    setTimeout(() => {
      if (!this.dataMap && !this.dataRasio) { return; }   

      this.initData();
    }, 500);
  }

  onMapReady(map: L.Map): void {
    setTimeout(() => {
      this.map = map;
      
      this.map.removeControl(this.map.zoomControl);
      
      L.control.zoom({
        position: 'bottomleft'
      }).addTo(this.map);
    }, 0);
  }

  initData(): void {
    this.loading = true;

    this.dataMap?.subscribe((d: any) => {
      d.features = d.features.filter((content: any) => content.properties[this.dataFilters.level.parent] ==  this.dataFilters.kode)
      this.polygon = d;
    });

    this.dataRasio?.subscribe((d: any) => {
      this.dataset = d.data.map((content: any) => {
        return {
          kode_kab: content[this.dataFilters.level.kode],
          nama_kab: content[this.dataFilters.level.nama],
          terkonfirmasi: content[this.dataFilters.sebaranType.index][this.dataFilters.sebaranType.subindex]
        }
      });

      // this.dataset = d.data.content.map((content: any) => {
      //   return {
      //     kode_kab: content.kode_kab,
      //     nama_kab: content.nama_kab,
      //     terkonfirmasi: content[this.dataFilters.sebaranType.index]
      //   }
      // });

      // console.log(this.dataset);
      this.totalKasus = this.dataset.map(a => a.terkonfirmasi).reduce((a, b) => a + b);

      this.loading = false;
      this.createMap();
    })
  }

  createMap(): void {
    this.initMap();
    this.drawMap();
  }

  initMap(): void {
    const data = this.dataset;

    d3.selectAll('.leaflet-interactive').remove();
    d3.selectAll('.map-tooltip').remove();
    this.map.invalidateSize();

    this.color = d3
      .scaleLinear()
      .domain([
        d3.min(data, (d: any) => d.terkonfirmasi) as number, 
        d3.max(data, (d: any) => d.terkonfirmasi) as number
      ])
      .range(<any>['#f5f5f5', this.dataFilters.sebaranType.color]);
      
    this.tooltip = d3.select('body').append('div')
      .attr('class', 'tooltip map-tooltip')
      .style('opacity', 0);

    d3.select('.map-title')
      .style('background', this.dataFilters.sebaranType.color)
      .style('border-color', this.dataFilters.sebaranType.color);
  }

  drawMap(): void {
    const polygonWilayah = this.getPolygon();

    this.map.addLayer(polygonWilayah);
    this.map.fitBounds(polygonWilayah.getBounds());
  }

  getPolygon() {
    return L.geoJSON(this.polygon, {
      style: (feature) => {
        if (this.dataset.filter(d => (d.kode_kab == feature?.properties[this.dataFilters.level.polygon_kode])).length == 0) 
          return {}

        return {
          color: '#666',
          fillColor: this.color(this.dataset.filter(d => (d.kode_kab == feature?.properties[this.dataFilters.level.polygon_kode]))[0].terkonfirmasi),
          fillOpacity: 1,
          weight: 1
        }
      },
      onEachFeature: (feature, layer) => {
        if (this.dataset.filter(d => (d.kode_kab == feature?.properties[this.dataFilters.level.polygon_kode])).length == 0) 
          return;

        const that = this;

        layer.on('mouseover', (event: any) => {
          const selectedCity = this.dataset.filter(d => (d.kode_kab == feature?.properties[this.dataFilters.level.polygon_kode]))[0];
          
          this.tooltip.transition()
            .duration(100)
            .style('opacity', .9);

          this.tooltip.html('<b>' + feature?.properties[this.dataFilters.level.polygon_nama] + '</b><br/>Terkonfirmasi: ' + that.numberFormat(selectedCity.terkonfirmasi || 0))
            .style('left', (event.originalEvent.pageX + 10) + 'px')
            .style('top', (event.originalEvent.pageY - 28) + 'px');
        });

        layer.on('mouseout', (e) => {
          this.tooltip.transition()
            .duration(500)
            .style('opacity', 0);
        });
      }
    });
  }

  selectSebaranType(type: any): void {
    this.dataFilters.sebaranType = type;

    this.filterSebaran.emit(this.dataFilters);
  }

  numberFormat(d: any): string {
    return d.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.');
  }
}
