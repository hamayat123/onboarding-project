import { HttpHeaders } from '@angular/common/http';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { faAngleDoubleUp, faAngleDoubleDown, faInfoCircle, faAngleRight } from '@fortawesome/free-solid-svg-icons';
import { NgbCalendar } from '@ng-bootstrap/ng-bootstrap';
import { ChartJenisKelaminComponent } from 'src/app/charts/chart-jenis-kelamin/chart-jenis-kelamin.component';
import { ChartRasioTerkonfirmasiComponent } from 'src/app/charts/chart-rasio-terkonfirmasi/chart-rasio-terkonfirmasi.component';
import { ChartTerkonfirmasiComponent } from 'src/app/charts/chart-terkonfirmasi/chart-terkonfirmasi.component';
import { ChartUmurComponent } from 'src/app/charts/chart-umur/chart-umur.component';
import { MapSebaranPolygonComponent } from 'src/app/maps/map-sebaran-polygon/map-sebaran-polygon.component';
import { ApiEndpointsService } from 'src/app/services/api-endpoints.service';
import { ApiHttpService } from 'src/app/services/api-http.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  @ViewChild('chartRasioTerkonfirmasi') chartRasioTerkonfirmasi?: ChartRasioTerkonfirmasiComponent;
  @ViewChild('mapSebaranPolygon') mapSebaranPolygon?: MapSebaranPolygonComponent;
  @ViewChild('chartJenisKelamin') chartJenisKelamin?: ChartJenisKelaminComponent;
  @ViewChild('chartUmur') chartUmur?: ChartUmurComponent;
  @ViewChild('chartTerkonfirmasi') chartTerkonfirmasi?: ChartTerkonfirmasiComponent;

  dataHarian: any;
  dataRekap: any;
  dataRasio: any;
  dataSebaran: any;
  dataWilayah: any;
  dataKecamatan: any;
  dataFilters = {
    kode: '32',
    level: {
      param: 'kota',
      kode: 'kode_kab',
      nama: 'nama_kab',
      polygon_kode: 'bps_kabupaten_kode',
      polygon_nama: 'kemendagri_kabupaten_nama',
      parent: 'bps_provinsi_kode'
    },
    sebaranType: {
      index : 'confirmation', 
      subindex : 'confirmation_diisolasi', 
      text: 'Positif - Isolasi / Dalam Perawatan',
      color: '#ae9420'
    }
  };
  
  dataFilterJenisKelamin = {
    index: 'confirmation',
    text: 'Terkonfirmasi'
  };

  dataFilterUmur = {
    index: 'confirmation',
    type: 'semua'
  };

  dataFilterHarian: any = {
    range: {
      from: {
        year: 2020,
        month: 3,
        day: 1
      },
      to: null
    }
  };

  dataJenisUmur: any = [{
    index: 'semua',
    text: 'Semua Umur'
  }, {
    index: 'anak',
    text: 'Anak'
  }]

  dataJenisChart: any = [{
    index: 'confirmation',
    text: 'Terkonfirmasi'
  }, {
    index: 'probable',
    text: 'Isolasi / Dalam Perawatan'
  }, {
    index: 'confirmation_selesai',
    text: 'Selesai Isolasi / Sembuh'
  }, {
    index: 'confirmation_meninggal',
    text: 'Meninggal'
  }];

  levelType: any = [{
    param: 'kota',
    kode: 'kode_kab',
    nama: 'nama_kab',
    polygon_kode: 'bps_kabupaten_kode',
    polygon_nama: 'kemendagri_kabupaten_nama',
    parent: 'bps_provinsi_kode'
  }, {
    param: 'kecamatan',
    kode: 'kode_kec',
    nama: 'nama_kec',
    polygon_kode: 'bps_kecamatan_kode',
    polygon_nama: 'kemendagri_kecamatan_nama',
    parent: 'bps_kabupaten_kode'
  }, {
    param: 'kelurahan',
    kode: 'kode_kel',
    nama: 'nama_kel',
    polygon_kode: 'bps_desa_kode',
    polygon_nama: 'kemendagri_desa_nama',
    parent: 'bps_kecamatan_kode'
  }]

  sebaranType: any = [{
    index : 'confirmation', 
    subindex : 'confirmation_diisolasi', 
    text: 'Positif - Isolasi / Dalam Perawatan',
    color: '#ae9420'
  }, {
    index : 'confirmation', 
    subindex : 'confirmation_selesai', 
    text: 'Positif - Selesai Isolasi / Sembuh',
    color: '#00441f'
  }, {
    index : 'confirmation', 
    subindex : 'confirmation_meninggal', 
    text: 'Positif - Meninggal',
    color: '#7d0005'
  }, {
    index : 'suspect', 
    subindex : 'suspect_diisolasi', 
    text: 'Suspek - Isolasi / Dalam perawatan',
    color: '#beab8d'
  }, {
    index : 'closecontact', 
    subindex : 'closecontact_dikarantina', 
    text: 'Kontak Erat - Masih Dikarantina',
    color: '#905a08'
  }, {
    index : 'probable', 
    subindex : 'probable_diisolasi', 
    text: 'Probable - Isolasi / Dalam Perawatan',
    color: '#ceb546'
  }, {
    index : 'probable', 
    subindex : 'probable_meninggal', 
    text: 'Probable - Meninggal',
    color: '#7d0005'
  }];

  faAngleDoubleUp = faAngleDoubleUp;
  faAngleDoubleDown = faAngleDoubleDown;
  faInfoCircle = faInfoCircle;
  faAngleRight = faAngleRight;

  activeTab = 1;
  selectedKota = '32';
  selectedKecamatan = '';
  selectedJenisKelamin = 0;
  selectedUmur = 0;
  selectedUmurType = 0;
  loading = true;

  constructor(
    private httpService: ApiHttpService,
    private endpointService: ApiEndpointsService,
    private calendar: NgbCalendar) {
      this.dataFilterHarian.range.to = calendar.getPrev(calendar.getToday(), 'd', 1);
  }

  ngOnInit(): void {
    this.dataHarian = this.httpService.get(this.endpointService.getRekapHarianEndpoint());
    this.dataRekap = this.httpService.get(this.endpointService.getRekapEndpoint());

    this.getKota();

    this.initData();
    this.loading = false;
  }

  initData(): void {
    this.httpService.get(this.endpointService.getWilayahEndpoint() + '?level=kabupaten').subscribe((d: any) => {
      this.dataWilayah = d.data.map((content: any) => {
        return {
          kode_bps: content.kode_bps,
          nama_wilayah: content.nama_wilayah
        }
      })
    });
  }

  getKota(): void {
    this.dataFilters.kode = '32';
    this.dataFilters.level = this.levelType[0];

    this.dataSebaran = this.httpService.get('assets/data/kotaV2.json');
    this.dataRasio = this.httpService.get(this.endpointService.getRekapPikobarEndpoint() + `?wilayah=${this.dataFilters.level.param}`, {
      headers: new HttpHeaders({
        'api-key': '480d0aeb78bd0064d45ef6b2254be9b3'
      })
    });
  }

  getKecamatan(id_bps: any): void {
    if (id_bps == '32') {
      this.dataKecamatan = [];

      return;
    }

    this.httpService.get(this.endpointService.getWilayahEndpoint() + `?level=kecamatan&id_bps=${id_bps}`).subscribe((d: any) => {
      this.dataKecamatan = d.data.map((content: any) => {
        return {
          kode_bps: content.kode_bps,
          nama_wilayah: content.nama_wilayah
        }
      })
    });

    this.dataFilters.kode = id_bps;
    this.dataFilters.level = this.levelType[1];

    this.dataSebaran = this.httpService.get('assets/data/kecamatanV2.json');
    this.dataRasio = this.dataRasio = this.httpService.get(this.endpointService.getRekapPikobarEndpoint() + `?wilayah=${this.dataFilters.level.param}&kode_kab=${id_bps}`, {
      headers: new HttpHeaders({
        'api-key': '480d0aeb78bd0064d45ef6b2254be9b3'
      })
    });

    setTimeout(() => {
      this.chartRasioTerkonfirmasi?.initData();
      this.mapSebaranPolygon?.initData();
    }, 500);
  }

  getKelurahan(id_bps: any): void {
    if (id_bps == '') {
      return;
    }

    this.dataFilters.kode = id_bps;
    this.dataFilters.level = this.levelType[2];
    
    this.dataSebaran = this.httpService.get('assets/data/kelurahanV2.json');
    this.dataRasio = this.dataRasio = this.httpService.get(this.endpointService.getRekapPikobarEndpoint() + `?wilayah=${this.dataFilters.level.param}&kode_kec=${id_bps}`, {
      headers: new HttpHeaders({
        'api-key': '480d0aeb78bd0064d45ef6b2254be9b3'
      })
    });

    setTimeout(() => {
      this.chartRasioTerkonfirmasi?.initData();
      this.mapSebaranPolygon?.initData();
    }, 500);
  }

  filterUmurJenisKelamin(jenis: any, value: any): void {
    if (jenis == 0) {   
      this.dataFilterJenisKelamin = this.dataJenisChart[value];

      setTimeout(() => {
        this.chartJenisKelamin?.initData();
      }, 500);
    } else if (jenis == 1) {
      this.dataFilterUmur.index = this.dataJenisChart[value].index;
      
      setTimeout(() => {
        this.chartUmur?.initData();
      }, 500);
    } else {
      this.dataFilterUmur.type = this.dataJenisUmur[value].index;
      
      setTimeout(() => {
        this.chartUmur?.initData();
      }, 500);
    }
  }

  filterHarianHandler(selectedDate: any): void {
    // console.log(selectedDate);
    this.dataFilterHarian.range.from = selectedDate.from;
    this.dataFilterHarian.range.to = selectedDate.to;
    
    setTimeout(() => {
      this.chartTerkonfirmasi?.initData();
    }, 500);
  }

  filterRasioHandler(childFilters: any): void {
    this.dataFilters = childFilters;
    
    setTimeout(() => {
      this.chartRasioTerkonfirmasi?.initData();
      this.mapSebaranPolygon?.initData();
    }, 500);
  }
}
