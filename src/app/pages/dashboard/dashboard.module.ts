import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardComponent } from './dashboard.component';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

import { ChartJenisKelaminComponent } from 'src/app/charts/chart-jenis-kelamin/chart-jenis-kelamin.component';
import { ChartTerkonfirmasiComponent } from 'src/app/charts/chart-terkonfirmasi/chart-terkonfirmasi.component';
import { ChartUmurComponent } from 'src/app/charts/chart-umur/chart-umur.component';
import { ChartRasioTerkonfirmasiComponent } from 'src/app/charts/chart-rasio-terkonfirmasi/chart-rasio-terkonfirmasi.component';
import { MapSebaranPolygonComponent } from 'src/app/maps/map-sebaran-polygon/map-sebaran-polygon.component';
import { ApiHttpService } from 'src/app/services/api-http.service';
import { ApiEndpointsService } from 'src/app/services/api-endpoints.service';
import { Constants } from 'src/app/config/constants';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';
import { FormsModule } from '@angular/forms';
import { DaterangepickerComponent } from 'src/app/components/daterangepicker/daterangepicker.component';

@NgModule({
  declarations: [
    DashboardComponent,
    ChartJenisKelaminComponent,
    ChartTerkonfirmasiComponent,
    ChartUmurComponent,
    ChartRasioTerkonfirmasiComponent,
    MapSebaranPolygonComponent,
    DaterangepickerComponent
  ],
  imports: [
    CommonModule,
    FontAwesomeModule,
    NgbModule,
    DashboardRoutingModule,
    LeafletModule,
    FormsModule
  ],
  providers: [
    ApiHttpService,
    ApiEndpointsService,
    Constants
  ]
})
export class DashboardModule { }
