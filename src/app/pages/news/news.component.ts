import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { DataBerita } from 'src/app/data/data-berita.model';

@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.scss']
})
export class NewsComponent implements OnInit {

  dataBerita: DataBerita[] = [];
  loading = true;

  constructor(private http: HttpClient) { }

  ngOnInit(): void {
    this.loading = true;

    this.http.get<DataBerita>('assets/data/data-berita.json').subscribe((d: any) => {
      this.dataBerita = d;
    });

    setTimeout(() => {
        this.loading = false;
    }, 500);
  }

}
