import { Injectable } from '@angular/core';
import { UrlBuilder } from '../shared/classes/url-builder';
import { QueryStringParameters } from '../shared/classes/query-string-parameters';
import { Constants } from 'src/app/config/constants';

@Injectable()
export class ApiEndpointsService {
  constructor(
    private constants: Constants
  ) {}

  private createUrl(action: string, isSecondAPI: boolean = false): string {
    const urlBuilder: UrlBuilder = new UrlBuilder(
      isSecondAPI
        ? this.constants.API_ENDPOINT_SECOND
        : this.constants.API_ENDPOINT,
      action
    );
    return urlBuilder.toString();
  }

  private createUrlWithQueryParameters(
    action: string,
    queryStringHandler?: (queryStringParameters: QueryStringParameters) => void
  ): string {
    const urlBuilder: UrlBuilder = new UrlBuilder(
      this.constants.API_ENDPOINT,
      action
    );
    if (queryStringHandler) {
      queryStringHandler(urlBuilder.queryString);
    }
    return urlBuilder.toString();
  }

  private createUrlWithPathVariables(
    action: string,
    pathVariables: any[] = []
  ): string {
    let encodedPathVariablesUrl: string = '';
    
    for (const pathVariable of pathVariables) {
      if (pathVariable !== null) {
        encodedPathVariablesUrl += `/${encodeURIComponent(
          pathVariable.toString()
        )}`;
      }
    }

    const urlBuilder: UrlBuilder = new UrlBuilder(
      this.constants.API_ENDPOINT,
      `${action}${encodedPathVariablesUrl}`
    );
    
    return urlBuilder.toString();
  }

  public getRekapHarianEndpoint(): string {
    return this.createUrl('rekapitulasi_v2/jabar/harian');
  }

  public getRekapEndpoint(): string {
    return this.createUrl('rekapitulasi_v2/jabar');
  }

  public getRekapPikobarEndpoint(): string {
    return this.createUrl('sebaran/pertumbuhan', true);
  }

  public getWilayahEndpoint(): string {
    return this.createUrl('wilayah/jabar');
  }
}
